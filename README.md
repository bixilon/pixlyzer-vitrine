# Pixlyzer Vitrine

(This is my first angular project, please don't blame me;)

This project is visualizing the data generated by [PixLyzer](https://gitlab.bixilon.de/bixilon/pixlyzer). It is wip and things may change over time.

## Screenshots

![](./doc/biomes.png)

## Planend things

- Visualizing all data
- Version selection (also reading own json files, etc)
- Diff tool
- Hosted version
