/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

import {Component, Input, OnInit} from '@angular/core';
import {SettingsService} from '../settings.service';

@Component({
    selector: 'app-version',
    templateUrl: './version.component.html',
    styleUrls: ['./version.component.css']
})

export class VersionComponent implements OnInit {
    public static SNAPSHOT_BIT_MASK = 1 << 30;

    constructor(public settings: SettingsService) {
    }

    @Input()
    public version: any;


    public formatProtocolVersion(value: number): string {
        // check if snapshot bit is set
        if ((value & VersionComponent.SNAPSHOT_BIT_MASK) !== 0){
            const snapshotId = value & ~VersionComponent.SNAPSHOT_BIT_MASK;
            return 'Snapshot ' + snapshotId + ' (' + value + ')';
        }
        return this.settings.formatValue(value);
    }

    ngOnInit(): void {
    }
}
