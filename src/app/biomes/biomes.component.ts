/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

import {Component, Input, OnInit} from '@angular/core';
import {KeyValue} from '@angular/common';
import {SettingsService} from '../settings.service';


@Component({
    selector: 'app-biomes',
    templateUrl: './biomes.component.html',
    styleUrls: ['./biomes.component.css'],
    providers: [SettingsService]
})
export class BiomesComponent implements OnInit {
    @Input()
    biomes: any;
    @Input()
    total: any;

    currentItems: any;

    itemCount = 0;

    constructor(public settings: SettingsService) {
    }

    originalOrder = (a: KeyValue<any, any>, b: KeyValue<any, any>): number => {
        return 0;
    };

    ngOnInit(): void {
        const entries = Object.entries(this.biomes);
        this.currentItems = entries.slice(0, 10);
        this.itemCount = entries.length;
    }

    public onPageChange($event): void {
        this.currentItems = Object.entries(this.biomes).slice($event.pageIndex * $event.pageSize, $event.pageIndex * $event.pageSize + $event.pageSize);
    }
}
