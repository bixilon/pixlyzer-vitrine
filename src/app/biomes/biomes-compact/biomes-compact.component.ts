/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';

@Component({
    selector: 'app-biomes-compact',
    templateUrl: './biomes-compact.component.html',
    styleUrls: ['./biomes-compact.component.css']
})
export class BiomesCompactComponent implements OnInit, AfterViewInit {
    @Input()
    biomes: any;
    @Input()
    total: any;

    displayedColumns: string[] = ['resource_location', 'id', 'depth', 'scale', 'temperature', 'downfall', 'water_color', 'fog_color', 'water_fog_color', 'sky_color', 'foliage_color_override', 'grass_color_override', 'category', 'precipitation', 'grass_color_modifier', 'temperature_modifier'];
    dataSource: MatTableDataSource<any>;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor() {
    }

    ngOnInit(): void {
        this.dataSource = new MatTableDataSource<any>(Object.entries(this.biomes));
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }


}
