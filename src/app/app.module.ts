/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import {VersionComponent} from './version/version.component';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {SimplePropertyComponent} from './version/simple-property/simple-property.component';
import {BiomesComponent} from './biomes/biomes.component';
import {BiomeComponent} from './biomes/biome/biome.component';
import {ColorPickerModule} from 'ngx-color-picker';
import {MatListModule} from '@angular/material/list';
import {ColorDisplayComponent} from './color-display/color-display.component';
import {CardPropertyComponent} from './card-property/card-property.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {SettingsService} from './settings.service';
import {BiomesCompactComponent} from './biomes/biomes-compact/biomes-compact.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        VersionComponent,
        SimplePropertyComponent,
        BiomesComponent,
        BiomeComponent,
        ColorDisplayComponent,
        CardPropertyComponent,
        BiomesCompactComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatTabsModule,
        MatIconModule,
        MatCardModule,
        MatDatepickerModule,
        MatInputModule,
        FormsModule,
        ColorPickerModule,
        MatListModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        HttpClientModule,
    ],
    providers: [SettingsService, HttpClient],
    bootstrap: [AppComponent]
})
export class AppModule {

}
