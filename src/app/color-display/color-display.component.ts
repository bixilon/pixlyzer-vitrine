/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-color-display',
    templateUrl: './color-display.component.html',
    styleUrls: ['./color-display.component.css']
})
export class ColorDisplayComponent implements OnInit {
    @Input()
    color: any;

    hexColor: string;

    constructor() {
    }

    ngOnInit(): void {
        if (typeof this.color === 'number') {
            this.hexColor = this.intToColor(this.color);
        } else if (this.color == null) {
            this.hexColor = '#FFFFFF';
        } else {
            this.hexColor = '#' + this.color.toString();
        }
    }

    intToColor(int: number): string {
        return '#' + int.toString(16).padStart(6, '0');
    }

    // thanks https://stackoverflow.com/questions/11867545/change-text-color-based-on-brightness-of-the-covered-background-area
    getContrastColor(): string {
        const hexColor = this.hexColor.replace('#', '');
        const r = parseInt(hexColor.substr(0, 2), 16);
        const g = parseInt(hexColor.substr(2, 2), 16);
        const b = parseInt(hexColor.substr(4, 2), 16);
        const yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
        return (yiq >= 128) ? 'black' : 'white';
    }

}
