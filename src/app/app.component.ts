/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

import {Component} from '@angular/core';

import {SettingsService} from './settings.service';
import {HttpClient} from '@angular/common/http';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [SettingsService, HttpClient],
})
export class AppComponent {
    data: any = {};
    title = 'PixLyzer Vitrine';

    constructor(public settings: SettingsService, private http: HttpClient) {
        const PIXLYZER_DATA_URL = 'https://gitlab.bixilon.de/bixilon/pixlyzer-data/-/raw/master/version/21w18a/all.min.json';

        console.log('Downloading pixlyzer data');
        const response = this.http.get(PIXLYZER_DATA_URL);
        response.toPromise().then((value => {
            console.log('Downloaded data!');
            this.data = value;
        }));
    }
}
