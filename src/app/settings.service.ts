/*
 * Minosoft
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

import {Injectable} from '@angular/core';
import {config} from 'rxjs';

@Injectable({
    providedIn: 'root'
})


class Config {
    compactMode = false;

    constructor() {
    }
}

export class SettingsService {
    public static config: Config = new Config();

    constructor() {
    }

    public getConfig(): Config {
        // @ts-ignore
        return config;
    }


    public formatValue(value: any): string {
        switch (value) {
            case true:
            case 'true':
                return '<span class="true">true</span>';
            case false:
            case 'false':
                return '<span class="false">false</span>';
        }
        return value.toString();
    }
}
